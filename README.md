# ESPHome I2C Relay Module Component
A Custom Component for ESPHome which will allow using a I2C Controlled Relay DDL from a Raspberry PI on an ESP32

## Description
C++ Header file and sample ESPHome Configuration to setup a working 4-Relay I2C board on an ESP32

## Installation & Usage
Copy the i2crelay.h file over to your ESPHome installation folder and then copy the code needed from the sample_config.yml to your ESP's configuration

## Support
I Offer no support, with luck this is fairly straight forward to get working for you

## Contributing
Merge Requests are welcome, but please test the code first and let me know what you did to test

## License
Open License

## Project status
I put this together in a couple hours and will likely be using it a bunch - ill be updating it as required for my use or to add features and test new relay boards

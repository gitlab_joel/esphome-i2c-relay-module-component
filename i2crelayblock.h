#include "esphome.h"

static const char *const TAG = "I2C Switch";
int ID = 0x00;

class I2CRelayBlock : public Component, public Switch {
  public:
    float get_setup_priority() const override { return esphome::setup_priority::BUS; }

  private:
    int deviceID;
    int busID;

  void setup() override {
    write_state(false);
    ESP_LOGCONFIG(TAG, "Setting up Switch '%s'...", this->name_.c_str());
  }

  void set_relayID(int newRelayID) {
    deviceID = newRelayID;
  }

  void set_busID(int newBusID) {
      busID = newBusID;
  }

  void write_state(bool state) override {
    // A terrible assumption that the Relayblock will always be at 0x10 :(
    // TODO: Make this selectable
    Wire.beginTransmission(busID);

    if (state == 1) {
      Wire.write(deviceID);
      Wire.write(0xFF);
    } else {
      Wire.write(deviceID);
      Wire.write(0x00);
    }

    Wire.endTransmission();
    this->publish_state(state);
  }
};